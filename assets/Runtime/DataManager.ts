/*
 * @Author: mjxjn@vip.qq.com
 * @Date: 2024-03-09 23:55:00
 * @LastEditors: mjxjn mjxjn@vip.qq.com
 * @LastEditTime: 2024-03-11 22:40:16
 * @FilePath: /Demo/assets/Runtime/DataManager.ts
 * @ApiFoxID: 
 * @Description: 
 */
import Singleton from "../Base/Singleton";
import { ITile } from "../Levels";

export default class DataManager extends Singleton {

    static get Instance () {
        return super.GetInstance<DataManager>();
    }

    mapInfo: Array<Array<ITile>>
    mapRowCount: number = 0
    mapColumnCount: number = 0
    levelIndex: number = 1

    reset() {
        this.mapInfo = []
        this.mapRowCount = 0
        this.mapColumnCount = 0
    }
}
