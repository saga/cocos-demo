/*
 * @Author: mjxjn@vip.qq.com
 * @Date: 2024-03-10 00:25:46
 * @LastEditors: mjxjn mjxjn@vip.qq.com
 * @LastEditTime: 2024-03-10 00:49:01
 * @FilePath: /Demo/assets/Runtime/ResourceManager.ts
 * @ApiFoxID: 
 * @Description: 
 */
import { _decorator, SpriteFrame, resources } from "cc";
import Singleton from "../Base/Singleton";

const { ccclass } = _decorator

@ccclass('ResourceManager')
export class ResourceManager extends Singleton {
    static get Instance () {
        return super.GetInstance<ResourceManager>();
    }

    public loadDir(path: string, type: typeof SpriteFrame = SpriteFrame) {
        return new Promise<SpriteFrame[]>((resolve, reject) => {
            resources.loadDir<SpriteFrame>(path, type, function (err, res) {
                if(err) {
                    reject(err);
                    return
                }
                resolve(res);
            });
        })
        
    }
}