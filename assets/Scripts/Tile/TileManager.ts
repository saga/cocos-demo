/*
 * @Author: mjxjn@vip.qq.com
 * @Date: 2024-03-09 22:47:20
 * @LastEditors: mjxjn mjxjn@vip.qq.com
 * @LastEditTime: 2024-03-10 00:12:44
 * @FilePath: /Demo/assets/Scripts/Tile/TileManager.ts
 * @ApiFoxID: 
 * @Description: 
 */
import { _decorator, Component, Layers, Node, resources, Size, Sprite, SpriteFrame, UITransform } from 'cc';
const { ccclass, property } = _decorator;
import Levels from '../../Levels';

export const TILE_WIDTH = 55;
export const TILE_HEIGHT = 55;

@ccclass('TileManager')
export class TileManager extends Component {
    init(spriteFrames: SpriteFrame, i: number, j: number){
        const sprite = this.addComponent(Sprite)
        sprite.spriteFrame = spriteFrames

        const transform = this.getComponent(UITransform);
        transform.setContentSize(new Size(TILE_WIDTH, TILE_HEIGHT))

        this.node.setPosition(i * TILE_WIDTH, -j * TILE_HEIGHT)
    }
}

