/*
 * @Author: mjxjn@vip.qq.com
 * @Date: 2024-03-09 22:47:20
 * @LastEditors: mjxjn mjxjn@vip.qq.com
 * @LastEditTime: 2024-03-11 23:10:39
 * @FilePath: /Demo/assets/Scripts/Tile/TileMapManager.ts
 * @ApiFoxID: 
 * @Description: 
 */
import { _decorator, Component, resources, SpriteFrame } from 'cc';
const { ccclass } = _decorator;
import { createUINode, randomByRange } from '../../Utils';
import { TileManager } from './TileManager';
import DataManager from '../../Runtime/DataManager';
import {ResourceManager} from '../../Runtime/ResourceManager';

@ccclass('TileMapManager')
export class TileMapManager extends Component {
    async init(){
        const spriteFrames = await ResourceManager.Instance.loadDir('texture/tile/tile/')
        const { mapInfo } = DataManager.Instance
        
        for (let i = 0; i < mapInfo.length; i++) {
            const column = mapInfo[i];
            for (let j = 0; j < column.length; j++) {
                const item = column[j];
                if(item.src === null || item.type === null) {
                    continue
                }
                
                let number = item.src
                if ((number === 1 || number === 5 || number === 9) && i % 2 === 0 && j % 2 === 1) {
                    number += randomByRange(1, 3)
                }
                
                const imgSrc = `tile (${number})`

                const node = createUINode()
                const spriteFrame = spriteFrames.find(v=>v.name === imgSrc) || spriteFrames[0]
                
                const tileManager = node.addComponent(TileManager)
                tileManager.init(spriteFrame, i, j)

                node.setParent(this.node);
            }
        }
        
    }

}

