/*
 * @Author: mjxjn@vip.qq.com
 * @Date: 2024-03-11 22:48:18
 * @LastEditors: mjxjn mjxjn@vip.qq.com
 * @LastEditTime: 2024-03-11 23:26:31
 * @FilePath: /Demo/assets/Scripts/Player/PlayerManager.ts
 * @ApiFoxID: 
 * @Description: 
 */
import { _decorator, Component, Node, Sprite, UITransform, Animation, AnimationClip, animation, SpriteFrame } from 'cc';
import { TILE_HEIGHT, TILE_WIDTH } from '../Tile/TileManager';
import { ResourceManager } from '../../Runtime/ResourceManager';
import { CONTROLLER_ENUM, EVENT_ENUM } from '../../Enum';
import EvenetManager from '../../Runtime/EventManager';
const { ccclass, property } = _decorator;

const ANIMATION_SPEED = 1/8

@ccclass('PlayerManager')
export class PlayerManager extends Component {

    x: number = 0
    y: number = 0
    targetX: number = 0
    targetY: number = 0

    private readonly speed = 1/10

    async init() {
        await this.render()

        EvenetManager.Instance.on(EVENT_ENUM.PLAYER_CTRL, this.move, this)
    }

    update() {
        this.updateXY()
        this.node.setPosition(this.x * TILE_WIDTH - TILE_WIDTH * 1.5, -this.y * TILE_HEIGHT + TILE_HEIGHT * 1.5)
    }

    updateXY() {
        if(this.targetX < this.x) {
            this.x -= this.speed
        }else if(this.targetX > this.x) {
            this.x += this.speed
        }

        if(this.targetY < this.y) {
            this.y -= this.speed
        }else if(this.targetY > this.y) {
            this.y += this.speed
        }

        if(Math.abs(this.targetX - this.x) <= 0.1 && Math.abs(this.targetY - this.y) <= 0.1) {
            this.x = this.targetX
            this.y = this.targetY
        }
    }

    move(inputDirection: CONTROLLER_ENUM) {
        switch (inputDirection) {
            case CONTROLLER_ENUM.TOP:
                this.targetY -= 1
                break;
            case CONTROLLER_ENUM.BOTTOM:
                this.targetY += 1
                break;
            case CONTROLLER_ENUM.LEFT:
                this.targetX -= 1
                break;
            case CONTROLLER_ENUM.RIGHT:
                this.targetX += 1
                break;
            default:
                break;
        }
    }

    async render() {
        const sprite = this.addComponent(Sprite)
        sprite.sizeMode = Sprite.SizeMode.CUSTOM

        const transform = this.getComponent(UITransform)
        transform.setContentSize(TILE_WIDTH *4, TILE_HEIGHT * 4)

        const spriteFrames = await ResourceManager.Instance.loadDir('texture/player/idle/top')
        const animationComponet = this.node.addComponent(Animation)

        const animationClip = new AnimationClip();
        
        const track = new animation.ObjectTrack()
        track.path = new animation.TrackPath().toComponent(Sprite).toProperty('spriteFrame');
        
        const frames: Array<[number, SpriteFrame]> = spriteFrames.map((item, index) => {
            return [ANIMATION_SPEED * index, item]
        })
        track.channel.curve.assignSorted(frames)

        animationClip.addTrack(track)
        animationClip.duration = frames.length * ANIMATION_SPEED
        animationClip.wrapMode = AnimationClip.WrapMode.Loop
        animationComponet.defaultClip = animationClip
        animationComponet.play()
    }
}