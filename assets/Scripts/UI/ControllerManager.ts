/*
 * @Author: mjxjn@vip.qq.com
 * @Date: 2024-03-09 22:47:20
 * @LastEditors: mjxjn mjxjn@vip.qq.com
 * @LastEditTime: 2024-03-11 23:32:07
 * @FilePath: /Demo/assets/Scripts/UI/ControllerManager.ts
 * @ApiFoxID: 
 * @Description: 
 */
import { _decorator, Component, Event } from 'cc';
import EvenetManager from '../../Runtime/EventManager';
import { CONTROLLER_ENUM, EVENT_ENUM } from '../../Enum';
const { ccclass, property } = _decorator;

@ccclass('ControllerManager')
export class ControllerManager extends Component {
    handleCtrl(evt: Event, type : string) {
        EvenetManager.Instance.emit(EVENT_ENUM.PLAYER_CTRL, type as CONTROLLER_ENUM)
    }
}

