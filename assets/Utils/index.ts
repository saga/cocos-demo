/*
 * @Author: mjxjn@vip.qq.com
 * @Date: 2024-03-09 23:29:53
 * @LastEditors: mjxjn mjxjn@vip.qq.com
 * @LastEditTime: 2024-03-10 00:52:18
 * @FilePath: /Demo/assets/Utils/index.ts
 * @ApiFoxID: 
 * @Description: 
 */

import { Node, UITransform, Layers } from 'cc';


export const createUINode = (name:string = '') => {
    const node = new Node(name)
    const transform = node.addComponent(UITransform);
    transform.setAnchorPoint(0,1)
    node.layer = 1 << Layers.nameToLayer("UI_2D")
    return node
}

export const randomByRange = (min:number, max:number) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
}