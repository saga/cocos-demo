/*
 * @Author: mjxjn@vip.qq.com
 * @Date: 2024-03-10 00:16:14
 * @LastEditors: mjxjn mjxjn@vip.qq.com
 * @LastEditTime: 2024-03-10 00:50:06
 * @FilePath: /Demo/assets/Base/Singleton.ts
 * @ApiFoxID: 
 * @Description: 
 */
export default class Singleton {
    private static _instance:any = null

    static GetInstance<T>(): T {
        if (this._instance === null) {
            this._instance = new this()
        }
        return this._instance
    }
}