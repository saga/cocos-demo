/*
 * @Author: mjxjn@vip.qq.com
 * @Date: 2024-03-09 22:11:59
 * @LastEditors: mjxjn mjxjn@vip.qq.com
 * @LastEditTime: 2024-03-11 22:37:19
 * @FilePath: /Demo/assets/Levels/index.ts
 * @ApiFoxID: 
 * @Description: 
 */
import level1 from './level1'
import level2 from './level2'

import { DIRECTION_ENUM, ENTITY_TYPE_ENUM, ENTITY_STATE_ENUM, TILE_TYPE_ENUM } from '../Enum'

export interface ITile {
    src: number | null
    type: TILE_TYPE_ENUM | null
}

export interface ILevel {
    mapInfo: Array<Array<ITile>>
    player: IEntity
    enemies: Array<IEntity>
    spikes: Array<ISpikes>
    bursts: Array<IEntity>
    door: IEntity
}

export interface IEntity {
    x: number
    y: number
    direction: DIRECTION_ENUM
    state: ENTITY_STATE_ENUM
    type: ENTITY_TYPE_ENUM
}

export interface ISpikes {
    x: number
    y: number
    type: ENTITY_TYPE_ENUM
    count: number
}

const levels: Record<string, ILevel> = {
    level1,
    level2,
}

export default levels